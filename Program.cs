﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz28._11_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int a;
            int number;
            int max = 0, min = 1000000;
            Console.Write("Enter: ");
            a = int.Parse(Console.ReadLine());
            while (i < a)
            {
                Console.Write("Enter number: ");
                number = int.Parse(Console.ReadLine());
                

                if (number > max)
                {
                    max = number;
                }
                if (number < min)
                {
                    min = number;
                }
                i++;

            }

            Console.WriteLine($"Max number: {max} and Min number: {min}");

            

            Console.ReadKey();
        }
    }
}
